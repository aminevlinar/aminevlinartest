﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml.Linq;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace AminevLinarTest
{
    public partial class Form1 : Form
    {
        /// <summary>
        /// Флаг того, что поиск запущен
        /// </summary>
        private bool Searching = false;
        private readonly string startToSearchString = "Начать поиск";
        private readonly string stopToSearchString = "Остановить поиск";
        private Stopwatch stopwatch;
        private System.Timers.Timer timer;
        private int filesFound;

        private Regex regexFilename;
        private Regex regexTextInFile;
        
        /// <summary>
        /// Разделитель пути
        /// </summary>
        private readonly char[] charSeparators = { '\\' };
        
        public Form1()
        {   
            InitializeComponent();
            LoadData();
            stopwatch = new Stopwatch();
            timer = new System.Timers.Timer();
            timer.Elapsed += UpdateTime;
            timer.Interval = 1000;
            timer.AutoReset = true;
            timer.Enabled = true;
            FormClosing += SaveData;
            backgroundWorker1.WorkerReportsProgress = true;
            backgroundWorker1.DoWork += BackgroundWorker1_DoWork;
            backgroundWorker1.RunWorkerCompleted += BackgroundWorker1_WorkerCompleted;
            backgroundWorker1.ProgressChanged += BackgroundWorker1_ChangeProgress;
            backgroundWorker1.WorkerSupportsCancellation = true;
            buttonPauseContinue.Enabled = false;
            buttonToStop.Enabled = false;
        }

        private void BackgroundWorker1_ChangeProgress(object sender, ProgressChangedEventArgs e)
        {
            labelQuantityFiles.Text = e.ProgressPercentage.ToString();
        }

        private void UpdateTime(object sender, EventArgs e)
        {
            labelElapsedTime.Invoke(new Action(() =>
            { labelElapsedTime.Text = stopwatch.Elapsed.ToString(@"hh\:mm\:ss"); }));
        }

        /// <summary>
        /// Действия фонового исполнения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorker1_DoWork(object sender, EventArgs e){
            try
            {
                filesFound = 0;
                stopwatch.Restart();
                timer.Start();
                regexFilename = new Regex(textBoxFileName.Text, RegexOptions.IgnoreCase);
                regexTextInFile = new Regex(textBoxTextInFile.Text, RegexOptions.IgnoreCase);
                Searching = true;
                buttonToStop.Invoke(new Action(() =>
                { buttonToStop.Enabled = true; }));
                buttonPauseContinue.Invoke(new Action(() =>
                { buttonPauseContinue.Enabled = true; }));
                buttonToStart.Invoke(new Action(() => { buttonToStart.Enabled = false; }));
                foreach (var textBox in Controls.OfType<TextBox>().Cast<TextBox>())
                    textBox.Invoke(new Action(() => { textBox.Enabled = false; }));
                buttonToChoseDirectory.Invoke(new Action(() => { buttonToChoseDirectory.Enabled = false; }));
                treeViewFoundFiles.Invoke(new Action(() => { treeViewFoundFiles.Nodes.Clear(); }));
                StartToSearch();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void BackgroundWorker1_WorkerCompleted(object sender, EventArgs e)
        {
            timer.Stop();
            labelCheckedFile.Text = "";
            Searching = false;
            buttonToStart.Enabled = true;
            buttonToStop.Enabled = false;
            foreach (var textBox in Controls.OfType<TextBox>().Cast<TextBox>())
                textBox.Enabled = true;
            buttonToChoseDirectory.Enabled = true;
            buttonPauseContinue.Enabled = false;
            stopwatch.Stop();
            labelElapsedTime.Text = stopwatch.Elapsed.ToString(@"hh\:mm\:ss");
        }
        /// <summary>
        /// Кнопка для запуска диалогового окна выбора стартовой директории
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonToChoseDirectory_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.Description = "Выберите стартовку папку, с которой необходимо начать поиск";
            folderBrowserDialog1.ShowDialog();
            if (!string.IsNullOrEmpty(folderBrowserDialog1.SelectedPath))
                textBoxChosenDirectory.Text = folderBrowserDialog1.SelectedPath;
        }

        /// <summary>
        /// Кнопка старта запуска поиска
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonToStart_Click(object sender, EventArgs e)
        {
            backgroundWorker1.RunWorkerAsync();
        }

        /// <summary>
        /// Загрузить начальные данные из xml
        /// </summary>
        private void LoadData()
        {
            try
            {
                XElement dataToLoad = XElement.Load("Settings.xml");
                foreach (var textBox in Controls.OfType<TextBox>().Cast<TextBox>())
                    textBox.Text = dataToLoad.Element(textBox.Name).Value;
            }
            catch { }
        }

        /// <summary>
        /// Сохранить данные в xml
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveData(object sender, EventArgs e)
        {
            try
            {
                FormClosing -= SaveData;
                XElement dataToSave = new XElement("Settings");
                foreach (var textBox in Controls.OfType<TextBox>().Cast<TextBox>())
                    dataToSave.Add(new XElement(textBox.Name, textBox.Text));

                dataToSave.Save("Settings.xml");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        /// <summary>
        /// Функция запуска поиска
        /// </summary>
        private void StartToSearch()
        {
            RecursiveSearch(textBoxChosenDirectory.Text);
        }

        /// <summary>
        /// Ресурсивный поиск
        /// </summary>
        /// <param name="path"></param>
        private void RecursiveSearch(string path)
        {
            if (backgroundWorker1.CancellationPending)
                return;
            try
            {
                var directories = Directory.GetDirectories(path);
                foreach (var directory in directories)
                {
                    while (!Searching) ;
                    if (backgroundWorker1.CancellationPending)
                        return;
                    RecursiveSearch(directory);
                }
            }
            catch
            {

            }
            try
            {
                var files = Directory.GetFiles(path);
                foreach (var file in files)
                {
                    while (!Searching) ;
                    if (backgroundWorker1.CancellationPending)
                        return;
                    if (FileSatisfy(file))
                        ShowFile(file);
                }
                    
            }
            catch
            {

            }
        }

        /// <summary>
        /// Файл удовлетворяет критериям поиска
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private bool FileSatisfy(string filename)
        {
            labelCheckedFile.Invoke(new Action(() =>
            { labelCheckedFile.Text = filename; }));
            var shortname = filename.Remove(0, filename.LastIndexOf("\\"));

            if (regexFilename.IsMatch(shortname))
                return true;

            using (StreamReader streamReader = new StreamReader(filename))
            {
                while (!streamReader.EndOfStream)
                {
                    string s = streamReader.ReadLine();
                    if (regexTextInFile.IsMatch(s))
                        return true;
                }
            }


            return false;
            //shortname.Contains(textBoxFileName.Text.ToLower());
        }
        
        /// <summary>
        /// Вывести файл в дерево
        /// </summary>
        /// <param name="path"></param>
        private void ShowFile(string path)
        {
            var shortPath = path.Remove(0, textBoxChosenDirectory.Text.Length);
            var splitedPath = shortPath.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
            filesFound++;
            backgroundWorker1.ReportProgress(filesFound);
            if (splitedPath.Length == 1)
            {
                treeViewFoundFiles.
                    Invoke(new Action(() => 
                    { treeViewFoundFiles.Nodes.Add(splitedPath[splitedPath.Length - 1]); }));
                return;
            }

            treeViewFoundFiles.Invoke(new Action(() =>
            { treeViewFoundFiles.AddNewNode(splitedPath); }));
            
        }

        private void buttonPauseContinue_Click(object sender, EventArgs e)
        {
            if (Searching)
            {
                Searching = false;
                buttonPauseContinue.Text = "Продолжить";
                stopwatch.Stop();
            }
            else
            {
                stopwatch.Start();
                Searching = true;
                buttonPauseContinue.Text = "Пауза";
            }
        }

        private void buttonToStop_Click(object sender, EventArgs e)
        {
            Searching = true;
            backgroundWorker1.CancelAsync();
        }
    }
}
