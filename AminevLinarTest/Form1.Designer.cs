﻿namespace AminevLinarTest
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.buttonToChoseDirectory = new System.Windows.Forms.Button();
            this.labelChosenDirectoryText = new System.Windows.Forms.Label();
            this.textBoxChosenDirectory = new System.Windows.Forms.TextBox();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.labelFileName = new System.Windows.Forms.Label();
            this.textBoxTextInFile = new System.Windows.Forms.TextBox();
            this.labelTextInFile = new System.Windows.Forms.Label();
            this.treeViewFoundFiles = new System.Windows.Forms.TreeView();
            this.labelFoundFiles = new System.Windows.Forms.Label();
            this.buttonToStart = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.labelElapsedTimeText = new System.Windows.Forms.Label();
            this.labelElapsedTime = new System.Windows.Forms.Label();
            this.labelFoundedFilesQuantityText = new System.Windows.Forms.Label();
            this.labelQuantityFiles = new System.Windows.Forms.Label();
            this.buttonPauseContinue = new System.Windows.Forms.Button();
            this.buttonToStop = new System.Windows.Forms.Button();
            this.labelChekedFileText = new System.Windows.Forms.Label();
            this.labelCheckedFile = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonToChoseDirectory
            // 
            this.buttonToChoseDirectory.Location = new System.Drawing.Point(675, 29);
            this.buttonToChoseDirectory.Name = "buttonToChoseDirectory";
            this.buttonToChoseDirectory.Size = new System.Drawing.Size(121, 29);
            this.buttonToChoseDirectory.TabIndex = 0;
            this.buttonToChoseDirectory.Text = "Выбрать папку";
            this.buttonToChoseDirectory.UseVisualStyleBackColor = true;
            this.buttonToChoseDirectory.Click += new System.EventHandler(this.ButtonToChoseDirectory_Click);
            // 
            // labelChosenDirectoryText
            // 
            this.labelChosenDirectoryText.AutoSize = true;
            this.labelChosenDirectoryText.Location = new System.Drawing.Point(12, 37);
            this.labelChosenDirectoryText.Name = "labelChosenDirectoryText";
            this.labelChosenDirectoryText.Size = new System.Drawing.Size(160, 17);
            this.labelChosenDirectoryText.TabIndex = 1;
            this.labelChosenDirectoryText.Text = "Стартовая директория";
            // 
            // textBoxChosenDirectory
            // 
            this.textBoxChosenDirectory.Location = new System.Drawing.Point(222, 32);
            this.textBoxChosenDirectory.Name = "textBoxChosenDirectory";
            this.textBoxChosenDirectory.Size = new System.Drawing.Size(440, 22);
            this.textBoxChosenDirectory.TabIndex = 2;
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(222, 60);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.Size = new System.Drawing.Size(440, 22);
            this.textBoxFileName.TabIndex = 4;
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Location = new System.Drawing.Point(12, 65);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(151, 17);
            this.labelFileName.TabIndex = 3;
            this.labelFileName.Text = "Шаблон имени файла";
            // 
            // textBoxTextInFile
            // 
            this.textBoxTextInFile.Location = new System.Drawing.Point(222, 88);
            this.textBoxTextInFile.Name = "textBoxTextInFile";
            this.textBoxTextInFile.Size = new System.Drawing.Size(440, 22);
            this.textBoxTextInFile.TabIndex = 6;
            // 
            // labelTextInFile
            // 
            this.labelTextInFile.AutoSize = true;
            this.labelTextInFile.Location = new System.Drawing.Point(12, 93);
            this.labelTextInFile.Name = "labelTextInFile";
            this.labelTextInFile.Size = new System.Drawing.Size(210, 17);
            this.labelTextInFile.TabIndex = 5;
            this.labelTextInFile.Text = "Текст, содержащийся в файле";
            // 
            // treeViewFoundFiles
            // 
            this.treeViewFoundFiles.Location = new System.Drawing.Point(15, 140);
            this.treeViewFoundFiles.Name = "treeViewFoundFiles";
            this.treeViewFoundFiles.Size = new System.Drawing.Size(1036, 235);
            this.treeViewFoundFiles.TabIndex = 7;
            // 
            // labelFoundFiles
            // 
            this.labelFoundFiles.AutoSize = true;
            this.labelFoundFiles.Location = new System.Drawing.Point(39, 120);
            this.labelFoundFiles.Name = "labelFoundFiles";
            this.labelFoundFiles.Size = new System.Drawing.Size(133, 17);
            this.labelFoundFiles.TabIndex = 8;
            this.labelFoundFiles.Text = "Найденные файлы";
            // 
            // buttonToStart
            // 
            this.buttonToStart.Location = new System.Drawing.Point(433, 381);
            this.buttonToStart.Name = "buttonToStart";
            this.buttonToStart.Size = new System.Drawing.Size(146, 35);
            this.buttonToStart.TabIndex = 9;
            this.buttonToStart.Text = "Начать поиск";
            this.buttonToStart.UseVisualStyleBackColor = true;
            this.buttonToStart.Click += new System.EventHandler(this.ButtonToStart_Click);
            // 
            // labelElapsedTimeText
            // 
            this.labelElapsedTimeText.AutoSize = true;
            this.labelElapsedTimeText.Location = new System.Drawing.Point(20, 385);
            this.labelElapsedTimeText.Name = "labelElapsedTimeText";
            this.labelElapsedTimeText.Size = new System.Drawing.Size(50, 17);
            this.labelElapsedTimeText.TabIndex = 10;
            this.labelElapsedTimeText.Text = "Время";
            // 
            // labelElapsedTime
            // 
            this.labelElapsedTime.AutoSize = true;
            this.labelElapsedTime.Location = new System.Drawing.Point(85, 385);
            this.labelElapsedTime.Name = "labelElapsedTime";
            this.labelElapsedTime.Size = new System.Drawing.Size(64, 17);
            this.labelElapsedTime.TabIndex = 11;
            this.labelElapsedTime.Text = "00:00:00";
            // 
            // labelFoundedFilesQuantityText
            // 
            this.labelFoundedFilesQuantityText.AutoSize = true;
            this.labelFoundedFilesQuantityText.Location = new System.Drawing.Point(608, 384);
            this.labelFoundedFilesQuantityText.Name = "labelFoundedFilesQuantityText";
            this.labelFoundedFilesQuantityText.Size = new System.Drawing.Size(120, 17);
            this.labelFoundedFilesQuantityText.TabIndex = 12;
            this.labelFoundedFilesQuantityText.Text = "Найдено файлов";
            // 
            // labelQuantityFiles
            // 
            this.labelQuantityFiles.AutoSize = true;
            this.labelQuantityFiles.Location = new System.Drawing.Point(790, 384);
            this.labelQuantityFiles.Name = "labelQuantityFiles";
            this.labelQuantityFiles.Size = new System.Drawing.Size(16, 17);
            this.labelQuantityFiles.TabIndex = 13;
            this.labelQuantityFiles.Text = "0";
            // 
            // buttonPauseContinue
            // 
            this.buttonPauseContinue.Location = new System.Drawing.Point(191, 381);
            this.buttonPauseContinue.Name = "buttonPauseContinue";
            this.buttonPauseContinue.Size = new System.Drawing.Size(143, 34);
            this.buttonPauseContinue.TabIndex = 14;
            this.buttonPauseContinue.Text = "Пауза";
            this.buttonPauseContinue.UseVisualStyleBackColor = true;
            this.buttonPauseContinue.Click += new System.EventHandler(this.buttonPauseContinue_Click);
            // 
            // buttonToStop
            // 
            this.buttonToStop.Location = new System.Drawing.Point(896, 381);
            this.buttonToStop.Name = "buttonToStop";
            this.buttonToStop.Size = new System.Drawing.Size(146, 35);
            this.buttonToStop.TabIndex = 15;
            this.buttonToStop.Text = "Остановить поиск";
            this.buttonToStop.UseVisualStyleBackColor = true;
            this.buttonToStop.Click += new System.EventHandler(this.buttonToStop_Click);
            // 
            // labelChekedFileText
            // 
            this.labelChekedFileText.AutoSize = true;
            this.labelChekedFileText.Location = new System.Drawing.Point(205, 120);
            this.labelChekedFileText.Name = "labelChekedFileText";
            this.labelChekedFileText.Size = new System.Drawing.Size(139, 17);
            this.labelChekedFileText.TabIndex = 16;
            this.labelChekedFileText.Text = "Проверяемый файл";
            // 
            // labelCheckedFile
            // 
            this.labelCheckedFile.AutoSize = true;
            this.labelCheckedFile.Location = new System.Drawing.Point(359, 120);
            this.labelCheckedFile.Name = "labelCheckedFile";
            this.labelCheckedFile.Size = new System.Drawing.Size(0, 17);
            this.labelCheckedFile.TabIndex = 17;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1063, 450);
            this.Controls.Add(this.labelCheckedFile);
            this.Controls.Add(this.labelChekedFileText);
            this.Controls.Add(this.buttonToStop);
            this.Controls.Add(this.buttonPauseContinue);
            this.Controls.Add(this.labelQuantityFiles);
            this.Controls.Add(this.labelFoundedFilesQuantityText);
            this.Controls.Add(this.labelElapsedTime);
            this.Controls.Add(this.labelElapsedTimeText);
            this.Controls.Add(this.buttonToStart);
            this.Controls.Add(this.labelFoundFiles);
            this.Controls.Add(this.treeViewFoundFiles);
            this.Controls.Add(this.textBoxTextInFile);
            this.Controls.Add(this.labelTextInFile);
            this.Controls.Add(this.textBoxFileName);
            this.Controls.Add(this.labelFileName);
            this.Controls.Add(this.textBoxChosenDirectory);
            this.Controls.Add(this.labelChosenDirectoryText);
            this.Controls.Add(this.buttonToChoseDirectory);
            this.Name = "Form1";
            this.Text = "AminevLinarTest";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button buttonToChoseDirectory;
        private System.Windows.Forms.Label labelChosenDirectoryText;
        private System.Windows.Forms.TextBox textBoxChosenDirectory;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.TextBox textBoxTextInFile;
        private System.Windows.Forms.Label labelTextInFile;
        private System.Windows.Forms.TreeView treeViewFoundFiles;
        private System.Windows.Forms.Label labelFoundFiles;
        private System.Windows.Forms.Button buttonToStart;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label labelElapsedTimeText;
        private System.Windows.Forms.Label labelElapsedTime;
        private System.Windows.Forms.Label labelFoundedFilesQuantityText;
        private System.Windows.Forms.Label labelQuantityFiles;
        private System.Windows.Forms.Button buttonPauseContinue;
        private System.Windows.Forms.Button buttonToStop;
        private System.Windows.Forms.Label labelChekedFileText;
        private System.Windows.Forms.Label labelCheckedFile;
    }
}

