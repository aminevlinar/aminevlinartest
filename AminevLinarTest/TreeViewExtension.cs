﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AminevLinarTest
{
    public static class TreeViewExtension
    {
        /// <summary>
        /// Добавление нового узла
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="splitedPath"></param>
        public static void AddNewNode(this TreeView treeView, string[] splitedPath)
        {
            if (!treeView.Nodes.ContainsKey(splitedPath[0]))
                treeView.Nodes.Add(splitedPath[0], splitedPath[0]);
            var currentNode = treeView.Nodes[treeView.Nodes.IndexOfKey(splitedPath[0])];
            for (int i = 1; i < splitedPath.Length - 1; i++)
            {
                if (!currentNode.Nodes.ContainsKey(splitedPath[i]))
                    currentNode.Nodes.Add(splitedPath[i], splitedPath[i]);
                currentNode = currentNode.Nodes[currentNode.Nodes.IndexOfKey(splitedPath[i])];
            }
            currentNode.Nodes.Add(splitedPath[splitedPath.Length - 1]);
        }
    }
}
